﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerBot : MonoBehaviour {

public int[] owners = new int[]{0,0,0,0,0,0,0,0,0};
public int actualPlayer = 1;
private int nowPlayer = 1;
public int winner = 0;
public GameObject Text;
private Text text;
public bool reset = false;
public GameObject ResetButton;
public Text stand;
private int winsOne;
private int winsTwo;
public Text Name1;
public Text Name2;
private bool found = false;

	// Use this for initialization
	void Start () {
		text = Text.GetComponent<Text>();
		// owners =new int[]{1,2,1,2,1,2,2,1,2};
	}
	
	// Update is called once per frame
	void Update () {
		if(actualPlayer == 1)
		text.text = "Spieler " + Name1.text + " ist dran";
		else
		text.text = "Spieler " + Name2.text + " ist dran";

	if(winner == 0){
		for (int i=0;i<3;i++ ){
		if(owners[i] == owners[i+3] && owners[i] == owners[i+6]){
			winner = owners[i];
			}
			}

		for (int y=0;y<6;y += 3){
		if (owners[y] == owners[y+1] && owners[y] == owners[y+2]){
			winner = owners[y];
		}
			}
		
		if (owners[0] == owners[4] && owners[0] == owners[8]){
			winner = owners[0];
		}
			
		if (owners[2] == owners[4] && owners[2] == owners[6]){
			winner = owners[2];
			
		}

		for(int i=0; i < owners.Length;i++){
			if(owners[i] == 0){
				found  = true;}
			else if(i == owners.Length-1 && found == false){
				ResetButton.SetActive(true);
				}
		}
		found = false;
		}
		else if(winner == 1){
			text.text = "Sieger ist Spieler " + Name1.text;
			ResetButton.SetActive(true);
		}
		else if(winner == 2){
			text.text = "Sieger ist Spieler " + Name2.text;
			ResetButton.SetActive(true);
	}
	
	

	stand.text = winsOne + " : "+ winsTwo;

	}	
	
	public int Check(int fieldNr){
		if(reset)
			reset =false;
		if(owners[fieldNr-1] == 0){
			owners[fieldNr-1] = actualPlayer;
			nowPlayer = actualPlayer;

			if(actualPlayer == 1){
				actualPlayer++;}
			else{
				actualPlayer--;}

			return nowPlayer;
		}
		else
		{
			return 0;
		}


	}

	public void resetGame(){
		if(winner == 1)
				winsOne++;
			else if(winner == 2)
				winsTwo++;
		reset = true;

		if(actualPlayer == 1){
			actualPlayer++;}
		else{
			actualPlayer--;}
		
		for(int i = 0;i < owners.Length;i++){
			owners[i] = 0;

		}
		winner = 0;
		ResetButton.SetActive(false);
	}

	public bool resetField(){
		if(reset)
			return true;
		else
			return false;

	}

}
